class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :user_id, :null => false
      t.string :message
      t.string :action, :null => false
      t.integer :action_user_id, :null => false
      t.boolean :unread, :default => true, :null => false
      t.integer :notifiable_id, :null => false
      t.string :notifiable_type, :null => false

      t.timestamps
    end
  end
end
