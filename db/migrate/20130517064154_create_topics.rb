class CreateTopics < ActiveRecord::Migration
  def change
    create_table :topics do |t|
      t.string :title, :null => false
      t.text :content
      t.integer :node_id, :null => false
      t.integer :creator_id, :null => false
      t.integer :comments_count
      t.boolean :block_comments, :null => false, :default => false
      t.string :last_replied_by, :default => ''

      t.timestamps
    end
  end
end
