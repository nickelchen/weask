class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.string :real_name
      t.string :address
      t.string :school
      t.integer :user_id
      
      t.timestamps
    end
  end
end
