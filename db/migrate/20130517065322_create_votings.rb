class CreateVotings < ActiveRecord::Migration
  def change
    create_table :votings do |t|
      t.integer :creator_id, :null => false
      t.integer :votable_id, :null => false
      t.string :votable_type, :null => false
      t.boolean :is_up, :null => false

      t.timestamps
    end
  end
end
