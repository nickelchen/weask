module WeAsk
  module Acts
    module Notifiable

      module ClassMethods
        def act_as_notifiable
          class_eval do
            has_many :notifications, :as => :notifiable
          end
        end
      end

      module InstanceMethods
        def collect_notifications

        end

        def send_notifications
          collect_notifications.each do |nf|
            Notification.notify *nf
          end
        end 
      end

      def self.included(base)
        base.extend ClassMethods
        base.send :include, InstanceMethods
      end

    end
  end
end