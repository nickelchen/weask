class Mailer < ActionMailer::Base
  default from: "support@weask.com"
  layout 'mailer'

  def test_email(user)
    @user = user
    mail to: @user.email, subject: 'Testing Email'
  end
end
