# encoding: utf-8
module ApplicationHelper

  def large_avatar(user)
    image_tag user.avatar.url(:large), :class => :large_avatar, :alt => "#{user.nickname} large avatar"
  end

  def medium_avatar(user)
    image_tag user.avatar.url(:medium), :class => :medium_avatar, :alt => "#{user.nickname} medium avatar"
  end

  def thumb_avatar(user)
    image_tag user.avatar.url(:thumb), :class => :thumb_avatar, :alt => "#{user.nickname} thumb avatar"
  end


  # def content_for(name, content = nil, &block)
  #   @has_content ||= {}
  #   @has_content[name] = true
  #   super(name, content, &block)
  # end

  # def has_content?(name)
  #   (@has_content && @has_content[name]) || false
  # end

  # def sidebar_content?
  #   has_content?(:sidebar)
  # end

  # def footer_content?
  #   has_content?(:footer)
  # end

  def render_admin_sidebar
    html = "".html_safe
    menus = [{ caption: '用户管理', url: admin_users_url, id: 'users'}, 
             { caption: '节点管理', url: admin_nodes_url, id: 'nodes' },
             { caption: '帖子管理', url: admin_topics_url, id: 'topics'},
             { caption: '评论管理', url: admin_comments_url, id: 'comments'},]
    selected = params[:controller].gsub('admin/', '')
    menus.each do |menu|
      html << content_tag("li", 
        link_to(menu[:caption], menu[:url]), :class => ( selected == menu[:id] ? 'active' : '' ) )
    end
    html
  end


end
