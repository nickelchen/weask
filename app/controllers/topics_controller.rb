class TopicsController < ApplicationController

  before_filter :authenticate_user!, :except => [:index, :show]


  # GET /topics
  # GET /topics.json
  def index
    @topics = Topic.all.sort_by(&:ranking).reverse

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @topics }
    end
  end

  # GET /topics/1
  # GET /topics/1.json
  def show
    @topic = Topic.find(params[:id])
    @comment = Comment.new({ commentable_type: Topic.to_s, commentable_id: @topic.id })

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @topic }
    end
  end

  # GET /topics/new
  # GET /topics/new.json
  def new
    @topic = Topic.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @topic }
    end
  end

  # GET /topics/1/edit
  def edit
    @topic = Topic.find(params[:id])
  end

  # POST /topics
  # POST /topics.json
  def create
    @topic = Topic.new(params[:topic])
    @topic.creator = current_user

    respond_to do |format|
      if @topic.save
        format.html { redirect_to @topic, notice: 'Topic was successfully created.' }
        format.json { render json: @topic, status: :created, location: @topic }
      else
        format.html { render action: "new" }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /topics/1
  # PUT /topics/1.json
  def update
    @topic = Topic.find(params[:id])

    respond_to do |format|
      if @topic.update_attributes(params[:topic])
        format.html { redirect_to @topic, notice: 'Topic was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topics/1
  # DELETE /topics/1.json
  def destroy
    @topic = Topic.find(params[:id])
    @topic.destroy

    respond_to do |format|
      format.html { redirect_to topics_url }
      format.json { head :no_content }
    end
  end

  def bookmark
    @topic = Topic.find(params[:id])
    unless @topic.bookmarked_by?(current_user)
      bm = @topic.bookmarks.build
      bm.user = current_user
      bm.save
    end
    respond_to do |format|
      format.html { redirect_to @topic }
    end
  end

  def unbookmark
    @topic = Topic.find(params[:id])
    if @topic.bookmarked_by?(current_user)
      bm = Bookmark.where({ 
        user_id: current_user, bookmarkable_id: @topic })
      @topic.bookmarks.destroy(bm)
    end
    respond_to do |format|
      format.html { redirect_to @topic }
    end
  end

end
