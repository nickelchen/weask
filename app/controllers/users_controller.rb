# encoding: utf-8

class UsersController < ApplicationController
  # GET /users
  # GET /users.json
  def index
    @users = User.all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @users }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show
    @user = User.find(params[:id])
    Mailer.test_email(@user).deliver
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/new
  # GET /users/new.json
  def new
    @user = User.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @user }
    end
  end

  # GET /users/1/edit
  def edit
    @user = User.find(params[:id])
    authorize! :edit, @user
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(params[:user])
    
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /users/1
  # PUT /users/1.json
  def update
    @user = current_user

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update_avatar
    @user = current_user
    respond_to do |format|
      if @user.update_without_password(params[:user])
        format.html { redirect_to @user, notice: '成功更新头像.' }
      else
        format.html { render :edit }
      end
    end
  end

  def update_account
    @user = current_user
    respond_to do |format|
      if @user.update_without_password(params[:user])
        format.html { redirect_to @user, notice: '成功更新个人信息.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :no_content }
    end
  end

  def follow
    @user = User.find(params[:id])
    current_user.follow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.json { head :no_content }
    end
  end

  def unfollow
    @user = User.find(params[:id])
    current_user.unfollow(@user)
    respond_to do |format|
      format.html { redirect_to @user }
      format.json { head :no_content }
      format.js {  }
    end
  end

  def remove_follower
    @user = User.find(params[:id])
    current_user.remove_follower(@user)
    respond_to do |format|
      format.html { redirect_to current_user }
      format.json { head :no_content }
    end
  end

  def notifications
    @unread = params[:unread] == '1'
    @user = User.find(params[:id])
    @notifications = @user.notifications @unread
    respond_to do |format|
      format.html {
        Notification.where(id: @notifications.collect(&:id)).update_all(:unread => false)
      }
    end
  end

  def add_bookmark
    @user = User.find(params[:id])
    bookmark = @user.bookmarks.build(bookmarkable_id: params[:bookmarkable_id])
    @saved = bookmark.save
    respond_to do |format|
      format.js { }
    end

  end

end
