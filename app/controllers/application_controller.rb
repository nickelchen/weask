# encoding: utf-8
class ApplicationController < ActionController::Base

  before_filter :_reload_libs, :if => :_reload_libs?


  def _reload_libs
    RELOAD_LIBS.each do |lib|
      require_dependency lib
    end
  end

  def _reload_libs?
    defined? RELOAD_LIBS
  end


  protect_from_forgery

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to '/users', :alert => exception.message
  end


  def render_403(options={})
    render_error({:message => '未授权', :status => 403}.merge(options))
    return false
  end

  def render_404(options={})
    render_error({:message => '文件未找到', :status => 404}.merge(options))
    return false
  end

  # Renders an error response
  def render_error(arg)
    arg = {:message => arg} unless arg.is_a?(Hash)

    @message = arg[:message]
    @status = arg[:status] || 500

    respond_to do |format|
      format.html {
        render :template => 'common/error', :layout => false, :status => @status
      }
      format.any { head @status }
    end
  end

  # TODO
  # render some message based on filter
  def render_filter_blocked_msg(msg)
    @message = msg
    respond_to do |format|
      format.js {
        #TODO render a alert error box here.
        render 'common/error'
      }
    end
  end


private 
  def after_sign_in_path_for(resource_or_scope)
    scope = Devise::Mapping.find_scope!(resource_or_scope)
    home_path = :"#{scope}_root_path"
    respond_to?(home_path, true) ? send(home_path) : root_url
  end
  
  def after_sign_out_path_for(resource_or_scope)
    scope = Devise::Mapping.find_scope!(resource_or_scope)
    home_path = :"#{scope}_root_path"
    respond_to?(home_path, true) ? send(home_path) : root_url
  end
 
end
