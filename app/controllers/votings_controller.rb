class VotingsController < ApplicationController

  before_filter :authenticate_user!, :find_votable, :check_not_voted_before, :build_new_voting
  
  def vote_up
    @voting.is_up = true
    respond_to do |format|
      format.js {
        flash[:notice] = @voting.save ? 'vote up successfully' : 'vote up failed'
        render 'vote'
      }
    end
  end

  def vote_down
    @voting.is_up = false
    respond_to do |format|
      format.js {
        flash[:notice] = @voting.save ? 'vote down successfully' : 'vote down failed'
        render 'vote'
      }
    end
  end

private
  def check_not_voted_before
    if current_user.voted?(@votable)
      render_filter_blocked_msg 'you have already voted'
      return false
    end
    true
  end

  def find_votable
    @votable = params[:votable_type].constantize.find(params[:votable_id])
  end

  def build_new_voting
    @voting = @votable.votings.build(params.slice(:votable_type, :votable_id))
    @voting.creator = current_user
  end
end
