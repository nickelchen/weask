class Admin::UsersController < ApplicationController
  layout "admin"
  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def toggle_blocked
    @user = User.find(params[:id])
    @user.update_attributes({ blocked: !@user.blocked})
    respond_to do |format|
      format.js
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      respond_to do |format|
        format.html { redirect_to admin_users_url }
      end
    else
      respond_to do |format|
        format.html { redirect_to edit_admin_user_url(@user) }
      end
    end

  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy
    respond_to do |format|
      format.html { redirect_to admin_users_url }
    end
  end
end
