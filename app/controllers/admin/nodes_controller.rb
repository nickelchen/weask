class Admin::NodesController < ApplicationController
  layout "admin"
  def index
    @nodes = Node.all
  end

  def new
    @node = Node.new
  end

  def edit
    @node = Node.find(params[:id])
  end

  def show
    @node = Node.find(params[:id])
  end

  def create
    @node = Node.create(params[:node])
    respond_to do |format|
      if @node.save
        format.html { redirect_to [:admin, @node], notice: 'Node was successfully created.'  }
        format.json { render json: @node, status: :created, location: @node  }
      else
        format.html { redirect_to action: 'new', error: 'Node created failed.'  }
        format.json { render json: @node.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @node = Node.find(params[:id])
    respond_to do |format|
      if @node.update_attributes(params[:node])
        format.html { redirect_to [:admin, @node], notice: 'Node updated successfully' }
      else
        format.html { render :edit , error: 'Node updated failed' }
      end    
    end    
  end

  def destroy
    @node = Node.find(params[:id])
    @node.destroy
    respond_to do |format|
      format.html { redirect_to admin_nodes_url }
    end
  end

end
