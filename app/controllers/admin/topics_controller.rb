class Admin::TopicsController < ApplicationController
  layout "admin"
  def index
    @topics = Topic.all
  end

  def edit
    @topic = Topic.find(params[:id])
  end

  def show
    @topic = Topic.find(params[:id])
  end

  def update
    @topic = Topic.find(params[:id])
    respond_to do |format|
      if @topic.update_attributes(params[:topic])
        format.html { redirect_to [:admin, @topic], notice: 'topic updated successfully' }
      else
        format.html { render :edit , error: 'topic updated failed' }
      end    
    end    
  end

  def destroy
    @topic = Topic.find(params[:id])
    @topic.destroy
    respond_to do |format|
      format.html { redirect_to admin_topics_url }
    end
  end
end
