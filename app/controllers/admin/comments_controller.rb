class Admin::CommentsController < ApplicationController
  layout "admin"
  def index
    @comments = Comment.all
  end

  def edit
    @comment = Comment.find(params[:id])
  end

  def show
    @comment = Comment.find(params[:id])
  end

  def update
    @comment = Comment.find(params[:id])
    respond_to do |format|
      if @comment.update_attributes(params[:comment])
        format.html { redirect_to [:admin, @comment], notice: 'comment updated successfully' }
      else
        format.html { render :edit , error: 'comment updated failed' }
      end    
    end    
  end

  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to admin_comments_url }
    end
  end
end
