# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
jQuery ->
  $('.comment-wrapper').mouseenter ->
    $(this).find('.reply-comment').show()
  .mouseleave ->
    $(this).find('.reply-comment').hide()
  $('.reply-comment').click ->
    $commentMeta = $(this).parent().find('.comment-creator')
    floor = $commentMeta.find('span.floor').text().trim()
    creator = $commentMeta.find('span.name').text().trim()
    $commentContent = $('[name="comment[content]"]')
    content = $commentContent.val()
    $commentContent.val( content + ' #' + floor + ' @' + creator + ' ' ).focus()
    moveCaretToEnd($commentContent[0])