#encoding: utf-8
class Comment < ActiveRecord::Base

  include WeAsk::Acts::Notifiable
  act_as_notifiable

  has_many :votings, as: :votable

  attr_accessible :commentable_id, :content, :creator_id, :commentable_type
  belongs_to :commentable, polymorphic: true
  belongs_to :creator, class_name: 'User'
  after_create :collect_notifications, :send_notifications

  def html_content
    content.gsub(/@\S+\s/) {|user| "<span class='at-user'>"+user+"</span>" }.gsub(/\#\d{1,}楼\s/) { |floor| "<span class='at-floor'>"+floor+"</span>"  }
  end

  def votings_up_count
    votings.select { |v| v.is_up }.size
  end

  def votings_down_count
    - votings.select { |v| !v.is_up }.size
  end

  def voted_by?(user)
    votings.any? { |v| v.creator == user }
  end

  def collect_notifications
    notifications = []

    # 通知提到的人
    mentioned_users = content.scan(/@(\S+) /).collect { |nickname|  User.find_by_nickname(nickname[0]) }
    mentioned_users.each do |user|
      notifications << [creator, commentable, 
        Notification::ACTION_MENTION, user, html_content ]
    end

    # 通知帖子的作者
    unless creator == commentable.creator || mentioned_users.include?(commentable.creator)
      notifications << [creator, commentable, 
        Notification::ACTION_REPLY, commentable.creator, html_content ] 
    end

    notifications
  end

end
