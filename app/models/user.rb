class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  # Setup accessible (or protected) attributes for your model
  attr_accessible :email, :password, :password_confirmation, :remember_me
  attr_accessible :avatar, :blocked, :email, :nickname, :reward, :role
  attr_accessible :account_attributes
  attr_accessible :followers, :followings


  mount_uploader :avatar, AvatarUploader

  has_one :account, :dependent => :destroy
  accepts_nested_attributes_for :account
  before_create :create_account

  has_many :topics, :foreign_key => 'creator_id', dependent: :destroy

  has_many :bookmarks, dependent: :destroy

  has_many :follower_relationships, class_name: 'Follow', foreign_key: :following_id, dependent: :destroy
  has_many :followers, through: :follower_relationships

  has_many :following_relationships, class_name: 'Follow', foreign_key: :follower_id, dependent: :destroy
  has_many :followings, through: :following_relationships


  def admin?
    nickname == 'aaaaa'
  end

  # user had already voted the votable object?
  def voted?(votable)
    votable.voted_by?(self)
  end

  def following?(user)
    followings.include?(user)
  end

  def followed_by?(user)
    followers.include?(user)
  end

  def follow(user)
    following_relationships.create(following_id: user.id) if !following?(user) and self != user
  end

  def unfollow(user)
    followings.delete(user) if following?(user)
  end

  def unread_notifications_count
    Notification.where(user_id: self.id, unread: true).count
  end
  def notifications(unread)
    Notification.where(user_id: self.id, unread: unread)
  end

  def remove_follower(user)
    followers.delete(user) if followed_by?(user)
  end

  def bookmarks_count
    bookmarks.count
  end

  def bookmark_topics
    bookmarks.select { |bm| bm.bookmarkable_tyep == 'Topic' }
  end



private 
  def create_account
    self.build_account if self.account.nil?
  end
    
end
