class Bookmark < ActiveRecord::Base
  attr_accessible :bookmarkable_id, :user_id

  belongs_to :user
  belongs_to :bookmarkable, polymorphic: true

end
