class Node < ActiveRecord::Base
  attr_accessible :desc, :name

  has_many :topics
end
