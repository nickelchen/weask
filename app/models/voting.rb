class Voting < ActiveRecord::Base
  attr_accessible :is_up, :creator_id, :votable_type, :votable_id

  belongs_to :creator, class_name: 'User'
  belongs_to :votable, polymorphic: true

end
