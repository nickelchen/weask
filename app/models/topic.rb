class Topic < ActiveRecord::Base
  attr_accessible :block_comments, :comments_count, :content, :last_replied_by, :node_id, :title, :creator_id

  belongs_to :node
  belongs_to :creator, class_name: 'User'

  validates_presence_of :node_id, :creator_id

  has_many :comments, as: :commentable
  has_many :votings, as: :votable

  has_many :bookmarks, as: :bookmarkable, dependent: :destroy
  has_many :interested_users, through: :bookmarks, source: :user

  RANKING_GRAVITY = 1.8


  def comments_count
    comments.size
  end

  def votings_up_count
    votings.select { |v| v.is_up }.size
  end

  def votings_down_count
    - votings.select { |v| !v.is_up }.size
  end

  def voted_by?(user)
    votings.any? { |v| v.creator == user }
  end

  def last_replied_by
    comments.last.creator.nickname if comments_count > 1
  end

  def linking
    "<a href='/topics/#{self.id}'>#{self.title}</a>"
  end

  def bookmarked_by?(user)
    interested_users.include?(user)
  end

  def ranking
    x = votings_up_count + votings_down_count
    y = x == 0 ? 0 : ( x > 0 ? 1 : -1 )
    z = x == 0 ? 1 : x.abs
    t = created_at - Time.new( 1970, 1, 1 )

    Math.log10(z) + ( y*t / 45000 )

  end

end
