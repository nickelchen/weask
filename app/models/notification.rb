#encoding: utf-8

class Notification < ActiveRecord::Base
  attr_accessible :message

  belongs_to :user
  belongs_to :action_user, class_name: 'User'
  belongs_to :notifiable, polymorphic: true

  ACTION_REPLY = 'reply'
  ACTION_MENTION = 'mention'

  def self.notify(action_user, notifiable, action, user, message)
    notify = Notification.new( message: message )
    notify.action_user = action_user
    notify.notifiable = notifiable
    notify.action = action
    notify.user = user
    notify.save
  end

  def notification_title
    case action
    when Notification::ACTION_MENTION
      "#{action_user.nickname} 在回复 #{notifiable.linking} 时提到你"
    when Notification::ACTION_REPLY
      "#{action_user.nickname} 在 #{notifiable.linking} 中回复你"
    end
  end

  def notification_message
    message
  end


end
