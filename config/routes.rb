WeAsk::Application.routes.draw do

  put "votings/vote_up/:votable_type/:votable_id" => 'votings#vote_up', :as => :vote_up
  put "votings/vote_down/:votable_type/:votable_id" => 'votings#vote_down', :as => :vote_down

  resources :notifications

  resources :topics do
    resources :comments do
      member do
        put 'vote'
      end
    end
    member do
      put 'vote'
      put 'bookmark', :as => :bookmark
      put 'unbookmark', :as => :unbookmark
    end
  end


  devise_for :users

  put 'users/update_avatar' => 'users#update_avatar', :as => :update_avatar
  put 'users/update_account' => 'users#update_account', :as => :update_account

  resources :users do
    member do
      put 'follow'
      put 'unfollow'
      put 'remove_follower'
      get 'notifications/(:unread)' => 'users#notifications', :as => :notifications
    end
  end
  namespace :admin do
    resources :users do
      member do
        put 'toggle_blocked'
      end
    end
    resources :nodes
    resources :topics
    resources :comments
  end

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'

  root :to => "topics#index"
  
end
